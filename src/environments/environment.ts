// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyBZyn6vViHZcmRhllnNXojm_hd1cxU8wDA",
    authDomain: "finaltest2021-6c01b.firebaseapp.com",
    projectId: "finaltest2021-6c01b",
    storageBucket: "finaltest2021-6c01b.appspot.com",
    messagingSenderId: "597787246969",
    databaseURL: "https://finaltest2021-6c01b.firebaseio.com"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
