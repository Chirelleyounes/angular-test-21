import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { stringify } from 'querystring';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private http:HttpClient,private db:AngularFirestore, private router:Router) { }

  cityCollection:AngularFirestoreCollection=this.db.collection('cities');
  apiuser = "https://api.openweathermap.org/data/2.5/weather?q="
  private url = "https://ju3m4eyzq7.execute-api.us-east-1.amazonaws.com/final"

  classify111(temp:number,fugtighed:number,):Observable<number>{
    const json = {
      "data":
        {"temp":temp,
          "fugtighed": fugtighed,
        }
    }
    console.log("jjjjj",json)
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res =>{
        let final = res.body
        return final; 
      })
    )
  }


  getdata(city:string):Observable<any>{
    let a:string = this.apiuser + city + "&appid=c8245ccac6cedd97a0f9c79502f8dbff&units=metric"
    return this.http.get(a);
  }

  deletecity(id:string){
    this.cityCollection.doc(id).delete();
  }

  updatecity(id:string,name:string,temp:string,fugtighed:string,wind:string,email:string,icon:string){
    const city =  {name:name, temp:temp, fugtighed:fugtighed, wind:wind,status:"after",email:email,icon:icon}
    this.cityCollection.doc(id).update(city)
  }

  addcity(name:string,userid:string){
    const city={name:name, userid:userid,status:"aa"};
    this.cityCollection.add(city).then(
      res =>{
        if (res) {
          this.router.navigate(['/citieslist']);
        }
      }
    )
  }
  
  getAll():Observable<any[]>{
    return this.cityCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
       const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    ); 
}
}
