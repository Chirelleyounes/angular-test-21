import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  userid:string;
  todos$:Observable<any>;
  constructor(private router:Router, public auth:AuthService, private todoserv:TodoService) { }

  addtodo(){
    this.router.navigate(['/formtodo']);
  }
  editvi(id:string){
    this.todoserv.updatevi(this.userid,id);
  }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        this.userid = user.uid;
        this.todos$ = this.todoserv.gettodo(this.userid);
       }
    )
  }

}
