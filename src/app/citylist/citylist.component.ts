import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { CityService } from '../city.service';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { rawListeners } from 'process';

@Component({
  selector: 'app-citylist',
  templateUrl: './citylist.component.html',
  styleUrls: ['./citylist.component.css']
})
export class CitylistComponent implements OnInit {

  dis = false;
  useremail:string;
  userid:string;
  textrain:string = "Predict";
  displayedColumns: string[] = ['No.','city','temp','icon','humidity','wind','email','predict'];
  dataSource =[];
  butn:string = "Predict";
  
  @ViewChild(MatTable,{static:true}) table: MatTable<any>;
  constructor(public auth:AuthService,private router:Router,public citysrv:CityService, private cdt: ChangeDetectorRef) { }

  getdata(city:string,ele,row){
    this.citysrv.getdata(city).subscribe(
      res =>{
        console.log(res);
        let test =  {id:ele.id ,name:ele.name, temp:res.main.temp, fugtighed:res.main.humidity, wind:res.wind.speed,status:"aaa",icon:"http://openweathermap.org/img/wn/"+res.weather[0].icon+".png"}
        console.log(test);
        this.dataSource[row] = test;
        this.table.renderRows();
      }
    )
  }

  predict(ele,index:string){
    this.citysrv.classify111(ele.temp,ele.fugtighed).subscribe(
      res => {
        ele.status = "prd"
        this.dataSource[index] = ele;
        this.table.renderRows();
        if (res <= 50) {
          this.textrain = "It's not raining"
        }else{
          this.textrain = "It's raining"
        }
      }
    )
  }

  Save(ele){
    this.citysrv.updatecity(ele.id,ele.name,ele.temp,ele.fugtighed,ele.wind,this.useremail,ele.icon)
  }

  delete(ele){
    this.citysrv.deletecity(ele.id);
  }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        if (user) {
          this.userid = user.uid;
          this.useremail = user.email;
          console.log(this.useremail);
          this.citysrv.getAll().subscribe(
            res =>{
              this.dataSource = res;
              console.log(this.dataSource)
            }
          )
        }else{
          this.router.navigate(['/login']);
        }
       }
    )
  }

}
