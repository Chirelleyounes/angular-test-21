import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-classifyform',
  templateUrl: './classifyform.component.html',
  styleUrls: ['./classifyform.component.css']
})
export class ClassifyformComponent implements OnInit {

  text:string;
  constructor(public classify:ClassifyService,private router:Router) { }

  onSubmit(){
    this.classify.doc = this.text;
    this.router.navigate(['/savedoc']);
  }

  ngOnInit(): void {
  }

}
