import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { ClassifyService } from '../classify.service';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-studentinfo',
  templateUrl: './studentinfo.component.html',
  styleUrls: ['./studentinfo.component.css']
})
export class StudentinfoComponent implements OnInit {

  email: string;
  psychometric: number;
  mathematics:number;
  salary:string;
  resultClassify
  resultpredict:string ="";
  show: boolean = false

  constructor(public auth:AuthService,public classify:ClassifyService,private stu:StudentService,private router:Router) { }

  cancel(){
    this.show =false;
  }
  save(){
    this.stu.addstudent(this.email,this.mathematics,this.psychometric,this.salary,this.resultpredict)
    this.router.navigate(['/Studenttable']);
  }

  onSubmit(){
    console.log("dddddddd")
    this.classify.classify111(this.psychometric,this.mathematics,this.salary).subscribe(
      res => {
        this.resultClassify = res;
        console.log('end of res  ',this.resultClassify);
        if (this.resultClassify < 0.5 ) {
          this.resultpredict = "fail"
          this.show = true;
        }else{
          this.resultpredict = "pass"
          this.show = true
        }
      }
    )

  }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        this.email = user.email;
      }
    )
  }

}
