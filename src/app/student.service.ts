import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http:HttpClient,private db:AngularFirestore) { }
  studentCollection:AngularFirestoreCollection=this.db.collection('student',ref => ref.orderBy('createdAt','desc'));

  addstudent(email:string, math:number, psy:number,cash:string,fail:string){
    const student={email:email, mathematics:math, psychometric:psy,tuition_pay:cash,fail:fail,createdAt:firebase.firestore.FieldValue.serverTimestamp()}
    this.studentCollection.add(student);
  }

  
  getAll():Observable<any[]>{
      return this.studentCollection.snapshotChanges().pipe(
        map(actions => actions.map(a => {
         const data = a.payload.doc.data();
          data.id = a.payload.doc.id;
          return { ...data };
        }))
      ); 
  }
  
  delete(id:string){
    this.db.doc(`student/${id}`).delete();
  }

  
}
