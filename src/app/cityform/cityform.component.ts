import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { CityService } from '../city.service';

@Component({
  selector: 'app-cityform',
  templateUrl: './cityform.component.html',
  styleUrls: ['./cityform.component.css']
})
export class CityformComponent implements OnInit {
  userid:string; 
  city:string;

  constructor(public auth:AuthService,private router:Router,public citysrv:CityService) { }

  onSubmit(){
    this.citysrv.addcity(this.city,this.userid)
  }

  ngOnInit(): void {
    this.auth.user.subscribe(
      user => {
        if (user) {
          this.userid = user.uid;
          this.citysrv.getAll().subscribe(
            res =>{
              console.log(res)
            }
          )
        }else{
          this.router.navigate(['/login']);
        }
       }
    )
  }

}
